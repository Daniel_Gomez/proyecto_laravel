@extends('layouts.app')

@section('content')
<div class="text-center">
<h1>Información</h1>
</div>
<div class="container">
<table class="table table-bordered">
<thead class="thead-dark">
<tr>
<th>ID</th>
<th>NOMBRE</th>
<th>APELLIDO PATERNO</th>
<th>APELLIDO MATERNO</th>
<th>FECHA NACIMIENTO</th>
</tr>

</thead>
<tbody>

@foreach($datos as $dato)
<tr>
<td>{{$dato->id}}</td>
<td>{{$dato->nombre}}</td>
<td>{{$dato->apellidopa}}</td>
<td>{{$dato->apellidoma}}</td>
<td>{{$dato->fechana}}</td>


</tr>
@endforeach

</tbody>
</table>
<a href="{{url('/datos/create')}}" class="btn btn-primary">Agregar</a>
</div>

@endsection