@extends('layouts.app')

@section('content')
<form action="{{route('datos.store')}}" method="post">
  @csrf
 <div class="container">
    <div class="form-group">
    <input type="text" class="form-control" name="nombre" placeholder="Nombre">

    </div>
    <div class="form-group">
    <input type="text" class="form-control" name="apellidopa" placeholder="Apellido materno">

    </div>
    <div class="form-group">
    <input type="text" class="form-control" name="apellidoma" placeholder="Apellido materno">

    </div>
    <div class="form-group">
    <input type="text" class="form-control" name="fechana" placeholder="Fecha de nacimiento">

    </div>
    <button type= "submit" class="btn btn-primary">Guardar</button>

</div>
 </form>

@endsection